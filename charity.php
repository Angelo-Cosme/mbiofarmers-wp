<?php
/**
 * Template name: Charity
 *
 * @package WordPress
 * @since mbiofarmers
 */

get_header(); ?>

<section id="slide-charity">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
      <p class="position-relative"><?php _e('Nous créons l\'harmonie et l\'équilibre entre l\'environnement et l\'économie', 'mbiofarmers'); ?></p>
        <h1>Nos bénéfices servent à donner <br> le sourire aux plus démunis.</h1>
      </div>
    </div>
  </div>
</section>

<section id="details" class="py-11">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <p>Le contenu sera bientôt disponible
        </p>
      </div>
    </div>
  </div>
</section>
  

<?php get_footer(); ?>
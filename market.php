<?php
/**
 * Template name: Market
 *
 * @package WordPress
 * @since mbiofarmers
 */

get_header();

$img_product = get_field('img_product');
  
?>

<section id="slide-market">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <p class="position-relative"><?php _e('Nous créons l\'harmonie et l\'équilibre entre l\'environnement et l\'économie', 'mbiofarmers'); ?></p>
        <h1><?php _e('Nos produits', 'mbiofarmers'); ?></h1>
      </div>
    </div>
  </div>
</section>

<section id="cta" class="py-7">
  <div class="container">
    <div class="row">
      <div class="col-xl-4 col-lg-4 col-md-6 mx-auto">
        <div class="d-flex align-items-center">
          <div class="item-img"><img src="<?php echo get_template_directory_uri(); ?>/images/clock.png" class="img-fluid" alt="Mbiofarmers"></div>
          <div class="detail">
            <h4><?php _e('Rapide & Simple', 'mbiofarmers'); ?></h4>
            <p><?php _e('Des prix avantageux et promotions en continu.', 'mbiofarmers'); ?></p>
          </div>
        </div>
      </div>
      <div class="col-xl-4 col-lg-4 col-md-6 mx-auto">
        <div class="d-flex align-items-center">
          <div class="item-img"><img src="<?php echo get_template_directory_uri(); ?>/images/premium.png" class="img-fluid" alt="Mbiofarmers"></div>
          <div class="detail">
            <h4><?php _e('Meilleure qualité', 'mbiofarmers'); ?></h4>
            <p><?php _e('Un large choix de produits  bio de qualité.', 'mbiofarmers'); ?></p>
          </div>
        </div>
      </div>
      <div class="col-xl-4 col-lg-4 col-md-6 mx-auto">
        <div class="d-flex align-items-center">
          <div class="item-img"><img src="<?php echo get_template_directory_uri(); ?>/images/package-box.png" class="img-fluid" alt="Mbiofarmers"></div>
          <div class="detail">
            <h4><?php _e('Expédition rapide', 'mbiofarmers'); ?></h4>
            <p><?php _e('La livraison rapide et un service client aux petits soins.', 'mbiofarmers'); ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="products" class="py-11">
  <div class="container">
    <div class="row">


    <?php
        $args = array(
          'post_type' => 'market',
          'orderby' => 'menu_order',
          'order' => 'ASC',
          'post_status' => 'publish',
          'post_per_page' => 12
        );
        $market_query = new WP_Query( $args );

        if ( $market_query->have_posts() ) { ?>
          <?php
          while ( $market_query->have_posts() ) {
            $market_query->the_post();
          ?>
            <div class="col-lg-4 col-md-6">
              <div class="product__all-single">
                <div class="product__all-img-box">
                  <div class="product__all-img">
                    <?php 
                      $img_product = get_field('img_product');
                      if( !empty( $img_product ) ): ?>
                          <img src="<?php echo esc_url($img_product['url']); ?>" class="img-fluid" alt="<?php echo esc_attr($img_product['alt']); ?>" />
                      <?php endif; ?>
                      <span class="product__all-sale"><?php the_field('seller'); ?></span>
                      <div class="product__all-img-icon">
                      <a href="<?php echo get_post_permalink($post->ID); ?>"><i class="bi bi-bag-plus"></i></a>
                    </div>
                  </div>
                </div>
                <h4><a href="<?php echo get_post_permalink($post->ID); ?>"><?php the_title(); ?></a></h4>
              </div>
            </div>
          <?php } ?>
        <?php }
        wp_reset_postdata();
        ?>

      
    </div>
  </div>
</section>
  

<?php get_footer(); ?>
<?php
/**
 * Template name: Blog
 *
 * @package WordPress
 * @since mbiofarmers
 */

get_header(); ?>



<section id="slide-blog">
  <div class="overlay d-flex justify-content-center align-items-center">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h1>Nos articles</h1>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="posts" class="py-11">
  <div class="container">
    <div class="title text-center">
      <h2>M'Biofarmers au quotidien</h2>
    </div>
    <div class="row">

            <?php
            $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
            $args = array(
              'post_type' => 'post',
              'posts_per_page' => 9,
              'paged' => $paged
            );
            $temp = $wp_query;
            $wp_query= null;
            $wp_query = new WP_Query($args);
            $i = 0;

            if ( $wp_query->have_posts() ) : ?>
              <?php while ( $wp_query->have_posts() ) :
                $wp_query->the_post(); ?>

                <div class="col-lg-4<?php echo ( 3 == $i ) ? : ''; ?>">
                  <div class="blog-one">
                    <div class="blog-one-img">
                      <?php the_post_thumbnail(); ?>
                      <div class="blog-date">
                        <span><?php the_time('d'); ?></span>
                        <p><?php the_time('Y'); ?></p>
                      </div>
                    </div>
                    <div class="blog-content">
                      <span><?php the_author(); ?></span>
                      <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
                    </div>
                  </div>
                </div>

              <?php endwhile; ?>
            <?php endif; ?>
    </div>


    <div class="paginate text-center">
      <?php
       if( function_exists('wp_paginate') ) {
         wp_paginate();
       }
      ?>
    </div>
  </div>
</section>


<?php get_footer(); ?>
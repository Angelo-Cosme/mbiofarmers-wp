<?php
/**
 * Template name: Services
 *
 * @package WordPress
 * @since mbiofarmers
 */

get_header(); ?>
 
<section id="slide-service">
  <div class="overlay d-flex justify-content-center align-items-center">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <p class="position-relative"><?php _e('Nous créons l\'harmonie et l\'équilibre entre l\'environnement et l\'économie', 'mbiofarmers'); ?></p>
          <h1 class="mt-5"><?php _e('Nous vous apportons des réponses', 'mbiofarmers'); ?></h1>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="details" class="py-11">
  <div class="container">
    <div class="row">
    </div>

    <div id="faq">
      <div class="container">
        <h2 class="text-center">FAQ</h2>
        <div class="row">
          <div class="col-lg-6">
            <div class="accrodion-grp faq-one-accrodion faq-one-accrodion-1" data-grp-name="faq-one-accrodion-1">
              <div class="accrodion active">
                <div class="accrodion-title">
                    <h4><?php _e('M’BIOFARMERS ?', 'mbiofarmers'); ?></h4>
                </div>
                <div class="accrodion-content">
                    <div class="inner">
                        <p><?php _e('
                        Est une ferme 100% bio et écologique qui dessert des entreprises alimentaires, 
                        pharmaceutiques et cosmétiques comme des consommateurs finaux. Elle dispose d’un 
                        supermarché en ligne de produits sains et durables jusqu’à 50% moins cher qu’en 
                        magasins bio. Nos produits sont approuvés par des experts de la santé pour que 
                        vous puissiez faire vos courses les yeux fermés.', 'mbiofarmers'); ?>
                        </p>
                    </div><!-- /.inner -->
                </div>
              </div>
              <div class="accrodion">
                <div class="accrodion-title">
                    <h4><?php _e('Pourquoi une adhésion ( dédiée uniquement aux consommateurs finaux?', 'mbiofarmers'); ?></h4>
                </div>
                <div class="accrodion-content">
                    <div class="inner">
                      <p>
                      <?php _e('L’adhésion annuelle nous permet de réduire nos marges afin de vous donner accès 
                      à des prix avantageux en tenant compte de votre propre panier. Nos réductions membres 
                      sont de 20% en moyenne par rapport aux magasins bio, et grâce à des actions “Bons plans” 
                      tout au long de l’année, ces remises grimpent régulièrement jusqu’à 50%.', 'mbiofarmers'); ?>
                      </p>
                    </div><!-- /.inner -->
                </div>
              </div>
              <div class="accrodion">
                <div class="accrodion-title">
                  <h4><?php _e('Combien coûte l\'adhésion ?', 'mbiofarmers'); ?></h4>
                </div>
                <div class="accrodion-content" style="display: none;">
                  <div class="inner">
                      <p>
                      <?php _e('L\'adhésion annuelle est de 200 dollars US /an, soit 16.66 dollars US/mois. Elle est 
                      valable un an et renouvelée chaque année automatiquement. Vous pouvez annuler le renouvellement 
                      à tout moment. Essayez notre service sans risque en souscrivant aujourd\'hui, et bénéficiez d\'un 
                      essai gratuit de 15 jours annulable à tout moment.', 'mbiofarmers'); ?>
                      </p>
                  </div><!-- /.inner -->
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="accrodion-grp faq-one-accrodion faq-one-accrodion-1" data-grp-name="faq-one-accrodion-1">
              <div class="accrodion active">
                <div class="accrodion-title">
                    <h4><?php _e('Est-ce que je vais vraiment faire des économies ?', 'mbiofarmers'); ?></h4>
                </div>
                <div class="accrodion-content">
                    <div class="inner">
                        <p>
                        <?php _e('On vous le garantit ! L’adhésion se rentabilise généralement en 3 commandes et nos membres 
                        économisent en moyenne 30 dollars US par commande, soit 360 dollars US par an. Si vous ne 
                        rentabilisez pas votre adhésion, nous vous remboursons la différence à la fin de l\'année en 
                        bon d’achat.', 'mbiofarmers'); ?>
                        </p>
                    </div><!-- /.inner -->
                </div>
              </div>
              <div class="accrodion">
                <div class="accrodion-title">
                    <h4><?php _e('Pourquoi est-ce que c’est mieux que d’aller en magasin ?', 'mbiofarmers'); ?></h4>
                </div>
                <div class="accrodion-content" style="display: none;">
                    <div class="inner">
                      <p>
                      <?php _e('Fini les heures perdues en magasin, faire vos courses n’a jamais été aussi simple grâce à M’BIOFARMERS! 
                        Filtrez les produits selon vos préférences et retrouvez vos favoris dans vos listes d’envies en un clic. 
                        Complétez votre commande en ligne en quelques minutes, et recevez-la chez vous.', 'mbiofarmers'); ?>
                      </p>
                    </div><!-- /.inner -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>
  

<?php get_footer(); ?>
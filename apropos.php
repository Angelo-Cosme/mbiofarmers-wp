<?php
/**
 * Template name: About
 *
 * @package WordPress
 * @since mbiofarmers 
 */

get_header(); 

$titleabout = get_field("titleabout");
 $desciption = get_field("desciption");

?>

<section id="slide-about">
  <div class="overlay d-flex justify-content-center align-items-center">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <p class="position-relative"><?php _e('Nous créons l\'harmonie et l\'équilibre entre l\'environnement et l\'économie', 'mbiofarmers'); ?></p>
          <h1><?php _e('À propos de M’BIOFARMERS', 'mbiofarmers'); ?></h1>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="about" class="py-11">
  <div class="container">
    <div class="row">
      <div class="col-lg-5">
      <?php if ( '' != $titleabout ) { ?>
        <h2><?php echo $titleabout; ?></h2>
        <?php } ?>
        <?php if ( '' != $desciption ) { ?>
        <p><?php echo $desciption; ?></p>
        <?php } ?>
        
      </div>
      <div class=" col-lg-7">
        <div class="img-abt">
          <img src="<?php echo get_template_directory_uri(); ?>/images/services-one-1-1.jpg" class="img-fluid" alt="Mbiofarmers">
          <img src="<?php echo get_template_directory_uri(); ?>/images/blog-img.jpeg" class="img-fluid tox-img" alt="">
        </div>
      </div>
    </div>
  </div>
</section>

<section id="count" class="py-11">
  <div class="container">
    <h2>Nos chiffres</h2>
    <div class="row">
      <div class="col-lg-3 mx-auto text-center">
        <h4><?php _e('+110', 'mbiofarmers'); ?></h4>
        <span><?php _e('Hectares de terre cultivée', 'mbiofarmers'); ?></span>
      </div>
      <div class="col-lg-3 mx-auto text-center">
        <h4><?php _e('+10', 'mbiofarmers'); ?></h4>
        <span><?php _e('Dix récoltes par année', 'mbiofarmers'); ?></span>
      </div>
      <div class="col-lg-3 mx-auto text-center">
        <h4><?php _e('+7', 'mbiofarmers'); ?></h4>
        <span><?php _e('Partenaires commerciaux', 'mbiofarmers'); ?></span>
      </div>
      <div class="col-lg-3 mx-auto text-center">
        <h4><?php _e('+160', 'mbiofarmers'); ?></h4>
        <span><?php _e('Employés dans nos champs', 'mbiofarmers'); ?></span>
      </div>
    </div>
  </div>
</section>



  

<?php get_footer(); ?>
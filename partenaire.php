<?php
/**
 * Template name: Partenaire
 *
 * @package WordPress
 * @since mbiofarmers
 */

get_header(); ?>

<section id="slide-about">
  <div class="overlay d-flex justify-content-center align-items-center">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <p class="position-relative">Nous créons l'harmonie et l'équilibre entre l'environnement et l'économie.</p>
          <h1>Contactez-Nous</h1>
        </div>
      </div>
    </div>
  </div>
</section>  

<section id="infoline" class="py-11">
  <div class="container">
    <div class="row">
      <div class="col-xl-4 col-lg-4 col-md-6 mx-auto">
        <div class="contact-item">
          <div class="item-img"><img src="<?php echo get_template_directory_uri(); ?>/images/infoline-1.png" class="img-fluid" alt="Mbiofarmers"></div>
          <div class="detail">
            <h4>Des questions ?</h4>
            <a href="#"><?php _e('(+229)57131321 - (+228)93619469', 'mbiofarmers'); ?></a>
          </div>
        </div>
      </div>
      <div class="col-xl-4 col-lg-4 col-md-6 mx-auto">
        <div class="contact-item">
          <div class="item-img"><img src="<?php echo get_template_directory_uri(); ?>/images/infoline-2.png" class="img-fluid" alt="Mbiofarmers"></div>
          <div class="detail">
            <h4>E-mail</h4>
            <a href="#"><?php _e('ceo@mbiofarmers.com', 'mbiofarmers'); ?></a>
          </div>
        </div>
      </div>
      <div class="col-xl-4 col-lg-4 col-md-6 mx-auto">
        <div class="contact-item">
          <div class="item-img"><img src="<?php echo get_template_directory_uri(); ?>/images/infoline-3.png" class="img-fluid" alt="Mbiofarmers"></div>
          <div class="detail">
            <h4>Localisation</h4>
            <a href="#"><?php _e('Cotonou - Bénin', 'mbiofarmers'); ?></a>
          </div>
        </div>
      </div>
    </div>
    <div class="line"></div>
  </div>
</section>

<section id="formular" class="py-11">
  <div class="container">
    <div class="title">
      <h2>Donnez nous plus informations sur votre organisation</h2>
    </div>
    <div class="row mt-7">
      <form action="#" method="POST" class="formular">
      <?php echo do_shortcode( '[[contact-form-7 id="136" title="Partenaire"]' ); ?>
      </form>
    </div>
  </div>
</section>



<?php get_footer(); ?>
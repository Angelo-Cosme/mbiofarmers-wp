<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @since mbiofarmers
 */
?>

<footer id="site-footer" class="py-11">
  <div class="footer-top">
    <div class="container">
      <div class="footer-inner">
        <div class="footer-shape"></div>
        <div class="row">
          <div class="col-lg-4">
            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.jpg" class="img-fluid" alt="Mbiofarmers">
            <p><?php _e('Ferme 100% bio et écologique qui dessert des entreprises alimentaires, pharmaceutiques 
            et cosmétiques comme des consommateurs finaux.', 'mbiofarmers'); ?></p>
          </div>
          <div class="col-lg-4">
            <h3><?php _e('En savoir plus', 'mbiofarmers'); ?></h3>
            <ul>
              <li><a href="<?php echo esc_url( home_url( '/partenaire/' ) ); ?>"><?php _e('Devenir partenaire', 'mbiofarmers'); ?></a></li>
              <li><a href="<?php echo esc_url( home_url( '/caritative/' ) ); ?>"><?php _e('Oeuvres caritatives', 'mbiofarmers'); ?></a></li>
              <li><a href="#"><?php _e('Conditions d\'utilisation', 'mbiofarmers'); ?></a></li>
              <li><a href="#"><?php _e('Politique de confidentialité', 'mbiofarmers'); ?></a></li>
            </ul>
          </div>
          <div class="col-lg-4">
            <h3><?php _e('Nous contacter', 'mbiofarmers'); ?></h3>
            <ul class="infoline">
              <li><i class="bi bi-envelope mr-3"></i><?php _e(' info@mbiofarmers.com', 'mbiofarmers'); ?></li>
              <li><i class="bi bi-geo-alt mr-3"></i><?php _e('Cotonou - Benin / Lomé - Togo', 'mbiofarmers'); ?></li>
              <li><i class="bi bi-telephone-inbound mr-3"></i><?php _e(' (+229)57131321 - (+228)93619469', 'mbiofarmers'); ?></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="footer-inner">
            <p class="text-center"><?php _e('© Copyright 2022 M’BIOFARMERS, Tous Droits Réservés.', 'mbiofarmers'); ?></p>
            <div class="bottom-scroll">
              <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="bi bi-arrow-up"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>

	
</div><!-- #page -->

	<?php wp_footer(); ?>
</body>
</html>

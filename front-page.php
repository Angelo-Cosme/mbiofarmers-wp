<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 * 
 * 
 * Template name: mbiofarmers ENTREPRISE FR 
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @since mbiofarmers 
 */

get_header(); 
 $title_welcome = get_field("title_welcome");
 $subtitle_welcome = get_field("subtitle_welcome");
 $welcome = get_field("welcome");


?>

<section id="slide">
  <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="false">
    <div class="carousel-indicators">
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
      <div class="slide-action">
        <p><?php _e('Nous créons l\'harmonie et l\'équilibre entre l\'environnement et l\'économie', 'mbiofarmers'); ?></p>
        <h1><?php _e('Le marché du Bio', 'mbiofarmers'); ?></h1>
        <a href="<?php echo esc_url( home_url( '/market-place/' ) ); ?>" class="btn-box">Découvrir <i class="bi bi-arrow-right"></i></a>
      </div>
      <div class="carousel-item active">
        <img src="<?php echo get_template_directory_uri(); ?>/images/main-slide-3-1.jpg" class="img-fluid" alt="Slide-BioFarmers">
      </div>
      <div class="carousel-item">
        <img src="<?php echo get_template_directory_uri(); ?>/images/main-slider-1-2.jpg" class="img-fluid" alt="Slide-BioFarmers">
      </div>
      <div class="carousel-item">
        <img src="<?php echo get_template_directory_uri(); ?>/images/main-slide-3-1.jpg" class="img-fluid" alt="Slide-BioFarmers">
      </div>
      <div class="feature-one">
        <div class="container">
          <div class="row">
            <div class="col-xl-4 col-md-4 col-lg-4">
              <div class="feature-one-single">
                <img src="<?php echo get_template_directory_uri(); ?>/images/farm.png" class="img-fluid" alt="Biofarmers">
                <div class="feature-content">
                  <h3><?php _e('Agriculture', 'mbiofarmers'); ?></h3>
                  <span><?php _e('Vos courses saines et durables sans vous ruiner.', 'mbiofarmers'); ?></span>
                </div>
              </div>
            </div>
            <div class="col-xl-4 col-md-4 col-lg-4">
              <div class="feature-one-single">
                <img src="<?php echo get_template_directory_uri(); ?>/images/field.png" class="img-fluid" alt="Biofarmers">
                <div class="feature-content">
                  <h3><?php _e('Leader', 'mbiofarmers'); ?></h3>
                  <span><?php _e('+23 produits approuvés par des experts.', 'mbiofarmers'); ?></span>
                </div>
              </div>
            </div>
            <div class="col-xl-4 col-md-4 col-lg-4">
              <div class="feature-one-single">
                <img src="<?php echo get_template_directory_uri(); ?>/images/harvest.png" class="img-fluid" alt="Biofarmers">
                <div class="feature-content">
                  <h3><?php _e('Production', 'mbiofarmers'); ?></h3>
                  <span><?php _e('Jusqu’à 50% moins cher qu’en magasin bio.', 'mbiofarmers'); ?></span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<section id="bio" class="py-11">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-5">
        <?php if ( '' != $title_welcome ) { ?>
          <h2><?php echo $title_welcome; ?></h2>
        <?php } ?>
        <?php if ('' != $subtitle_welcome) { ?>
        <span><?php echo $subtitle_welcome; ?></span>
        <?php } ?>
        <?php if ('' != $welcome) { ?>
        <p><?php echo $welcome; ?></p>
        <?php } ?>
        <a href="<?php echo esc_url( home_url( '/a-propos/' ) ); ?>" class="btn-box">En savoir plus <i class="bi bi-arrow-right"></i></a>
      </div>
      <div class="offset-1 col-lg-6 order-sm-1">
        <div class="side-slider">
          <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="true">
            <div class="carousel-indicators">
              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="<?php echo get_template_directory_uri(); ?>/images/Side-mbiofarmers-1.jpg" class="img-fluid" alt="Mbiofarmers">
              </div>
              <div class="carousel-item">
                <img src="<?php echo get_template_directory_uri(); ?>/images/Side-mbiofarmers-3-1.jpg" class="img-fluid" alt="Mbiofarmers">
              </div>
              <div class="carousel-item">
                <img src="<?php echo get_template_directory_uri(); ?>/images/Side-mbiofarmers-2.jpeg" class="img-fluid" alt="Mbiofarmers">
              </div>
            </div>
          </div>
          <div class="note-box"><h4><?php _e('Nos champs de production', 'mbiofarmers'); ?></h4></div>
        </div>
      </div>
    </div>
  </div>
</section>


<section id="farmers-img">
  <div class="advange-shape-one">
    <img src="<?php echo get_template_directory_uri(); ?>/images/about-one-shape-1.png" class="img-fluid" alt="Mbiofarmers">
  </div>
</section>
<section id="advanges" class="py-11">
  <div class="advange-bg position-absolute"></div>
  <div class="container">
    <div class="title text-center">
      <span><?php _e('Nous sommes une référence', 'mbiofarmers'); ?></span>
      <h2><?php _e('Pourquoi acheter chez Mbiofarmers?', 'mbiofarmers'); ?></h2>
    </div>
    <div class="row">
      <div class="col-lg-4">
        <div class="advange-single">
          <div class="img-box">
            <img src="<?php echo get_template_directory_uri(); ?>/images/services-one-3.jpg" class="img-fluid" alt="Mbiofarmers">
            <div class="icon-box">
              <img src="<?php echo get_template_directory_uri(); ?>/images/harvest.png" class="img-fluid" alt="">
            </div>
          </div>
          <div class="content-box">
            <h3><?php _e('Production maraîchère', 'mbiofarmers'); ?></h3>
            <p><?php _e('Nous valorisons les produits sur des marchés nationaux et internationaux en tant qu\'acteur de la filière.', 'mbiofarmers'); ?>
            </p>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="advange-single">
          <div class="img-box">
            <img src="<?php echo get_template_directory_uri(); ?>/images/services-one-2.jpeg" class="img-fluid" alt="Mbiofarmers">
            <div class="icon-box">
              <img src="<?php echo get_template_directory_uri(); ?>/images/harvest (2).png" class="img-fluid" alt="">
            </div>
          </div>
          <div class="content-box">
            <h3><?php _e('Transformation agroalimentaire', 'mbiofarmers'); ?></h3>
            <p><?php _e('Nous prolongeons la période de disponibilité des denrées alimentaires et ainsi de réduire les soudures.', 'mbiofarmers'); ?>
            </p>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="advange-single">
          <div class="img-box">
            <img src="<?php echo get_template_directory_uri(); ?>/images/services-one-1-1.jpg" class="img-fluid" alt="Mbiofarmers">
            <div class="icon-box">
              <img src="<?php echo get_template_directory_uri(); ?>/images/harvest (1).png" class="img-fluid" alt="">
            </div>
          </div>
          <div class="content-box">
            <h3><?php _e('Agriculture <br> durable', 'mbiofarmers'); ?></h3>
            <p><?php _e('Favoriser des écosystèmes sains et une gestion durable de la terre, de l\'eau et des ressources naturelles.', 'mbiofarmers'); ?>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="made-benin" class="pb-11">
  <div class="container">
    <div class="title text-center">
      <h2><?php _e('Produits 100% <span>Bio</span> <br> Made in <span>Bénin</span>', 'mbiofarmers'); ?></h2>
    </div>
    <div class="row">
      <div class="col-lg-3 mx-auto left">
        <div class="feature">
          <div class="item-feature"><img src="<?php echo get_template_directory_uri(); ?>/images/herbal-spa-treatment-leaves.png" alt=""><span><?php _e('Produits frais', 'mbiofarmers'); ?></span></div>
          <p><?php _e('Des produits sans additifs, cultivés sur des sols riches en minéraux naturels.', 'mbiofarmers'); ?></p>
        </div>
        <div class="feature">
          <div class="item-feature"><img src="<?php echo get_template_directory_uri(); ?>/images/health.png" alt=""><span><?php _e('Pour la santé', 'mbiofarmers'); ?></span></div>
          <p><?php _e('Acheter des produits Mbiofarmers, c\'est prendre soin de sa santé au quotidien.', 'mbiofarmers'); ?></p>
        </div>
        <div class="feature">
          <div class="item-feature"><img src="<?php echo get_template_directory_uri(); ?>/images/green-energy.png" alt=""><span><?php _e('Production écologique', 'mbiofarmers'); ?></span></div>
          <p><?php _e('Nous vous préservons de l\'impact négatif des produits chimiques sur votre santé à long terme.', 'mbiofarmers'); ?></p>
        </div>
      </div>
      <div class="col-lg-4 ship-product mx-auto text-center">
        <h3><?php _e('M\'BIOFARMERS <br> c\'est...', 'mbiofarmers'); ?></h3>
        <div class="share-product">
          <img src="<?php echo get_template_directory_uri(); ?>/images/share-product.png" class="img-fluid" alt="">
        </div>
      </div>
      <div class="col-lg-3 mx-auto right">
        <div class="feature">
          <div class="item-feature"><span><?php _e('Meilleure qualité', 'mbiofarmers'); ?></span><img src="<?php echo get_template_directory_uri(); ?>/images/premium.png" alt=""></div>
          <p><?php _e('Fournir des produits de premier choix à nos clients, reste toujours notre priorité au quotidien.', 'mbiofarmers'); ?></p>
        </div>
        <div class="feature">
          <div class="item-feature"><span><?php _e('Produits saisoniers', 'mbiofarmers'); ?></span><img src="<?php echo get_template_directory_uri(); ?>/images/season.png" alt=""></div>
          <p><?php _e('Nous assurons la disponibilité de nos produits en fonction des saisons de récolte tous nos clients.', 'mbiofarmers'); ?></p>
        </div>
        <div class="feature">
          <div class="item-feature"><span><?php _e('Service livraison', 'mbiofarmers'); ?></span><img src="<?php echo get_template_directory_uri(); ?>/images/package-box.png" alt=""></div>
          <p><?php _e('Chez Mbiofarmers, c\'est aussi la livraison à domicile ou en entreprise suivant votre position géographique.', 'mbiofarmers'); ?></p>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="banner" class="py-11">
  <div class="container">
    <div class="banner-content">
      <div class="shape-one">
        <img src="<?php echo get_template_directory_uri(); ?>/images/unbeatable-shape-1.png" class="img-fluid" alt="MBiofarmers">
      </div>
      <div class="shape-two">
        <img src="<?php echo get_template_directory_uri(); ?>/images/unbeatable-shape-2.png" class="img-fluid" alt="Mbiofarmers">
      </div>
      <h3><?php _e('Nous répondons à votre demande pour <br/> des exportations à grande échelle', 'mbiofarmers'); ?></h3>
      <div class="cta text-center">
        <a href="<?php echo esc_url( home_url( '/market-place/' ) ); ?>" class="btn btn-box">Découvrez nos produits <i class="bi bi-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>


    <?php
        $args = array(
          'posts_per_page' => 3
        );
        $blog_query = new WP_Query($args);
        $i = 0;

        if ( $blog_query->have_posts() ) : ?>
          <section id="recent-post" class="py-11">
            <div class="container">
              <div class="title text-center">
                <h2><?php _e('Les articles récents', 'mbiofarmers'); ?></h2>
                <span><?php _e('Mbiofarmers c\'est aussi un mélange des histoires du savoir-vivre et savoir-être.', 'mbiofarmers'); ?></span>
              </div>
              <div class="row">

              <?php while ( $blog_query->have_posts() ) :
                $blog_query->the_post(); ?>

                <div class="col-lg-4<?php echo ( 3 == $i ) ? : ''; ?>">
                  <div class="blog-one">
                    <div class="blog-one-img">
                      <?php the_post_thumbnail(); ?>
                      <div class="blog-date">
                        <span><?php the_time('d'); ?></span> -
                        <span><?php the_time('m'); ?></span> -
                        <p><?php the_time('Y'); ?></p>
                      </div>
                    </div>
                    <div class="blog-content">
                      <span><?php the_author(); ?></span>
                      <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
                    </div>
                  </div>
                </div>
                <?php $i++;
              endwhile; ?>
              </div> 
            </div>
          </section>
        <?php endif; ?>

    </div>
  </div>
</section>


  

  

<?php get_footer(); ?>
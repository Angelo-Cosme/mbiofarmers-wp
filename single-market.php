<?php
/**
 * Template Name: market page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since mbiofarmers 0.1
 *
*/

get_header(); 
$description = get_field("description");
$available = get_field("available");
$img_product = get_field('img_product');

?>


<section id="single-product" style="background:url(<?php echo esc_url($img_product['url']); ?>) center right no-repeat;">
  <div class="overlay d-flex justify-content-center align-items-center">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <div class="breacumb mx-auto w-25"> 
            <p>Accueil / Market place</p>
          </div>
          <h1><?php the_title(); ?></h1>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="product-details" class="py-11">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-xl-6">
      <?php 
        if( !empty( $img_product ) ): ?>
            <img src="<?php echo esc_url($img_product['url']); ?>" class="img-fluid" alt="<?php echo esc_attr($img_product['alt']); ?>" />
        <?php endif; ?>
      </div>
      <div class="col-lg-6 col-xl-6">
        <h3 class="product-details__title"><?php the_title(); ?></h3>
        <div class="line"></div>
        <div class="product-details__reveiw">
          <i class="bi bi-star-fill"></i>
          <i class="bi bi-star-fill"></i>
          <i class="bi bi-star-fill"></i>
          <i class="bi bi-star-fill"></i>
          <i class="bi bi-star-fill"></i>
        </div>
          
          <?php if ( '' != $description ) { ?>
            <p><?php echo $description; ?></p>
          <?php } ?>
          
        <div class="statut d-flex align-items-center mt-4">
          <div class="state mr-3"><i class="bi bi-universal-access-circle"></i></div>
          <div><?php if ( '' != $description ) { ?>
            <span><?php echo $available; ?></span>
          <?php } ?></div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="formular" class="py-11">
  <div class="container">
    <div class="title">
      <h2>Remplissez le formulaire pour passer votre commande</h2>
    </div>
    <div class="row mt-7">
      <form action="#" method="POST" class="formular">
      <?php echo do_shortcode( '[contact-form-7 id="131" title="commande"]' ); ?>
      </form>
    </div>
  </div>
</section>

<?php get_footer(); ?>

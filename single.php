<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @since Mbiofarmers 0.1
 */

get_header(); ?>
<section id="slide-post">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h1><?php the_title(); ?></h1>
      </div>
    </div>
  </div>
</section>

<!-- <section id="slide-post" style="background:url(  <?php the_post_thumbnail_url(); ?>) center center no-repeat;">
  <div class="overlay d-flex justify-content-center align-items-center">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <div class="breacumb mx-auto w-25"> 
            <p>Accueil / Blog</p>
          </div>
          <h1><?php the_title(); ?></h1>
        </div>
      </div>
    </div>
  </div>
</section> -->

<section id="detail-post" class="py-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-9 offset-2">
      <?php while ( have_posts() ) : the_post(); ?>
        <p>
          <?php get_template_part( 'content', get_post_format() ); ?>
        </p>
        <?php endwhile; ?>
      </div>
    </div>
  </div>
</section>

  <?php
    $args = array(
      'posts_per_page' => 3
    );
    $blog_query = new WP_Query($args);
    $i = 0;

    if ( $blog_query->have_posts() ) : ?>
    <section id="recent-post" class="py-5">
            <div class="container">
              <div class="title text-center">
                <h2><?php _e('À lire également', 'mbiofarmers'); ?></h2>
              </div>
              <div class="row">

              <?php while ( $blog_query->have_posts() ) :
                $blog_query->the_post(); ?>

                <div class="col-lg-4<?php echo ( 3 == $i ) ? : ''; ?>">
                  <div class="blog-one">
                    <div class="blog-one-img">
                      <?php the_post_thumbnail(); ?>
                      <div class="blog-date">
                        <span><?php the_time('d'); ?></span> -
                        <span><?php the_time('m'); ?></span> -
                        <p><?php the_time('Y'); ?></p>
                      </div>
                    </div>
                    <div class="blog-content">
                      <span><?php the_author(); ?></span>
                      <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
                    </div>
                  </div>
                </div>
                <?php $i++;
              endwhile; ?>
              </div> 
            </div>
    </section>
  <?php endif; ?>


<?php get_footer(); ?>
